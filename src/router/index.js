import { createRouter, createWebHistory } from "vue-router";
import Home from '@/pages/Home.vue';

export default createRouter({
    history: createWebHistory(),
    routes: [
        {
            name: 'Home',
            path:  '/',
            component: Home
        }
    ],
  })





